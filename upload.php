<?
    if (
        isset($_POST['limit']) && 
        is_numeric($_POST['limit']) && 
        $_POST['limit'] >= 1 && 
        $_POST['limit'] <= 3600
    ) {
        set_time_limit($_POST['limit']);
    } else {
        echo 'Ошибка параметра ограничителя.';
        exit;
    }

    if (!isset($_FILES['csv'])) {
        echo 'Ошибка: Файл не был загружен на сервер.';
        exit;
    }

    if ($_FILES['csv']['error'] > 0) {
        echo 'Ошибка при загрузке файла.<br/>';
        echo 'Код Ошибки: ' . $_FILES['csv']['error'];
        exit;
    }

    if (pathinfo($_FILES['csv']['name'], PATHINFO_EXTENSION) != 'csv') {
        echo 'Ошибка: Неверный тип файла.';
        exit;
    }

    $mysqli = new mysqli(
        'localhost', 
        'root', 
        '', 
        'test'
    );

    if ($mysqli->connect_errno) {
        echo 'Ошибка: Не удалсь создать соединение с базой MySQL.<br/>';
        echo 'Код Ошибки: ' . $mysqli->connect_errno . '<br/>';
        echo 'Ошибка: ' . $mysqli->connect_error;
        exit;
    }

    $table_codes = array();
    $sql = 'SELECT `ARTICUL` FROM `test`';

    if (!$result = $mysqli->query($sql)) {
        echo 'Ошибка при выполнении запроса "'.$sql.'".<br/>';
        echo 'Ошибка: ' . $mysqli->error;
        exit;
    }

    while ($row = $result->fetch_row()) {
        array_push($table_codes, $row[0]);
    }

    $filename = $_FILES['csv']['tmp_name'];
    $file_codes = array();
    $file_data = array();

    $log = fopen('log.txt', 'w+');
    
    $delimiters = [';', ','];
    $delimiter = htmlspecialchars($_POST['delimiter']);
    $delimiter = ($delimiter >= count($delimiters)) ? $delimiters[0] : $delimiters[$delimiter];

    fwrite($log, '['.date('d/m/Y в H:i:s').'] Считываем файл.'.PHP_EOL);
    if (($handle = fopen($filename, 'r')) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
            array_push($file_codes, $data[0]);
            array_push($file_data, $data);
        }
        fclose($handle);
    } else {
        echo 'Ошибка: Не удалось считать файл.';
        fwrite($log, '['.date('d/m/Y в H:i:s').'] Не удалось считать файл.'.PHP_EOL);
        exit;
    }
    $file_rows_count = count($file_codes);
    fwrite($log, '['.date('d/m/Y в H:i:s').'] Файл считан. Количество строк в файле: '.$file_rows_count.'.'.PHP_EOL);

    fwrite($log, '['.date('d/m/Y в H:i:s').'] Проверяем артикулы из БД на их наличие в файле.'.PHP_EOL);
    $row = 0;
    foreach ($table_codes as $code) {
        $index_table = array_search($code, $file_codes);
        if ($index_table === FALSE) {
            $row++;
            $sql = 'UPDATE `test` SET `COUNT` = 0 WHERE `ARTICUL` = "'.$code.'"';
            ($result = $mysqli->query($sql)) 
                ? fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'. Товар с артикулом "'.$code.'" не обнаружен в импортируемом файле. Его количество в БД обновлено до 0.'.PHP_EOL)
                : fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'. Ошибка при обновлении товара с артикулом "'.$code.'" в БД.'.PHP_EOL);
        }
    }
    fwrite($log, '['.date('d/m/Y в H:i:s').'] Проверка артикулов для БД завершена.'.PHP_EOL);

    fwrite($log, '['.date('d/m/Y в H:i:s').'] Импортируем товары из файла.'.PHP_EOL);
    $row = 0;
    foreach ($file_data as $data) {
        $row++;
        $sql = 'SELECT `ARTICUL` FROM `test` WHERE `ARTICUL` = "'.$data[0].'"';
        $result = $mysqli->query($sql);
        if (!$result = $mysqli->query($sql)) {
            fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'/'.$file_rows_count.'. Произошла ошибка при запросе "'.$sql.'".'.PHP_EOL);
        } else {
            if ($result->num_rows > 0) {
                $sql = 'UPDATE `test` SET `PRICE` = '.$data[1].', `COUNT` = '.$data[2].' WHERE `ARTICUL` = "'.$data[0].'"';
                ($result = $mysqli->query($sql)) 
                    ? fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'/'.$file_rows_count.'. Товар с артикулом "'.$data[0].'" обновлен в БД.'.PHP_EOL)
                    : fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'/'.$file_rows_count.'. Ошибка при обновлении товара с артикулом "'.$data[0].'" в БД.'.PHP_EOL);
            } else {
                $sql = 'INSERT INTO `test` (`ARTICUL`, `PRICE`, `COUNT`) VALUES ("'.$data[0].'",'.$data[1].','.$data[2].')';
                ($result = $mysqli->query($sql)) 
                    ? fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'/'.$file_rows_count.'. Товар с артикулом "'.$data[0].'" добавлен в БД.'.PHP_EOL)
                    : fwrite($log, '['.date('d/m/Y в H:i:s').'] '.$row.'/'.$file_rows_count.'. Ошибка при добавлении товара с артикулом "'.$data[0].'" в БД.'.PHP_EOL);
            }
        }
    }

    fwrite($log, '['.date('d/m/Y в H:i:s').'] Работа завершена'.PHP_EOL);
    fclose($log);
    echo 'Работа завершена.';