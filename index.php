<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Test</title>

        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form id="file-loader-form" enctype="multipart/form-data" method="POST" action="/upload.php">
                        <div class="form-group">
                            <label for="file-loader-form-file"><h2>Загрузите файл csv</h2></label>
                            <input type="file" name="csv" accept=".csv" class="form-control" id="file-loader-form-file">
                        </div>
                        <div class="form-group">
                            <label for="file-loader-form-delimiter">Разделитель</label>
                            <select class="form-control" name="delimeter" id="file-loader-form-delimiter">
                                <option value="0">Точка с запятой</option>
                                <option value="1">Запятая</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="file-loader-form-limit">Ограничение работы скрипта в сек</label>
                            <input type="number" name="limit" id="file-loader-form-limit" min="1" max="3600" value="30">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default" id="file-loader-form-import">Импортировать</button>
                        </div>
                    </form>
                    <hr>
                    <h3>Статус</h3>
                    <p id="status">Ожидаю загрузки файла.</p>
                </div>
            </div>
        </div>
        <script
            src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous">
        </script>
        <script>
            $(document).ready(function() {
                $('#file-loader-form').submit(function(event) {
                    event.preventDefault();
                    $('#file-loader-form-import').attr('disabled', true);
                    let ext = $('#file-loader-form-file').val().split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['csv']) == -1) {
                        $('#status').text('Ошибка: Неверный тип файла.');
                        $('#file-loader-form-import').attr('disabled', false);
                    } else {
                        $('#status').text('Работаем с файлом. Ожидайте. Лог находится в файле log.txt в корнеовой папке скрипта.');
                        let fd = new FormData();
                        let file = $('#file-loader-form-file').prop('files')[0];
                        let delimiter = $('#file-loader-form-delimiter').val();
                        let limit = $('#file-loader-form-limit').val();
                        fd.append('csv', file);
                        fd.append('delimiter', delimiter);
                        fd.append('limit', limit);
                        $.ajax({
                            url: '/upload.php',
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: fd,                         
                            type: 'post',
                            success: function(res) {
                                $('#status').html(res);
                                $('#file-loader-form-import').attr('disabled', false);
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>